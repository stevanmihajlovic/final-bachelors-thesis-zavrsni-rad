﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSP_IntVersion
{
    public class Individual
    {
        //genotype deo
        public List<int> genes { get; set; }
        public int geneLength;

        //phenotype deo
        public double fitnessDistance { get; set; } //fitness 1
        public double fitnessCost { get; set; } //fitness 2
        public int rank;
        public double crowdingDistance;
        public List<Individual> dominatesList;
        public int dominatedByCount;

        public Individual() 
        {
            fitnessDistance =0;
            fitnessCost=0;
            rank=0;
            crowdingDistance=0;
            dominatesList=new List<Individual>();
            dominatedByCount=0;
            genes = new List<int>();
            
        }
        public Individual(int gl) //random gl razlicitih brojeva pocev od 1 zbog matrica 
        {
            fitnessDistance = 0;
            fitnessCost = 0;
            rank = 0;
            crowdingDistance = 0;
            dominatesList = new List<Individual>();
            dominatedByCount = 0;

            genes = new List<int>();
            geneLength = gl;
            List<int> lookup = new List<int>();
            int i;
            for (i = 0; i < geneLength; i++)
                lookup.Add(i + 1);
            for (i = 0; i < geneLength; i++)
            {
                int temp = TSP.randomize.Next(lookup.Count);
                genes.Add(lookup[temp]);
                lookup.RemoveAt(temp);
            }
        }
        public Individual(Individual a)
        {
            dominatesList = new List<Individual>();
            genes = new List<int>();
            geneLength = a.geneLength;
            for(int i=0; i<geneLength;i++)
            {
                genes.Add(a.genes[i]);
            }
        }
        public void mutation()  //sansa za mutaciju je nad celom jedinkom i menjaju se samo dva random mesta, nije mesto po mesto
        {
            int firstIndex = TSP.randomize.Next(geneLength);
            int secondIndex = TSP.randomize.Next(geneLength);
            int temp = genes[firstIndex];
            genes[firstIndex] = genes[secondIndex];
            genes[secondIndex] = temp;
        }

        public void crossover(Individual a)  //od dva roditelja dobijamo samo jednog potomka
        {
            List<int> temp = new List<int>();
            int crossPoint = TSP.randomize.Next(geneLength);
            int i;
            for (i = 0; i < crossPoint; i++)
            {
                temp.Add(this.genes[i]);
            }
            for (int j = 0; j < geneLength;j++ )
            {
                if (!temp.Contains(a.genes[j]))
                    genes[i++] = a.genes[j];
            }
        }

        public void calculateFitness()
        {
            fitnessDistance = 0;
            fitnessCost = 0;
            for (int i = 0; i < geneLength - 1; i++)
            {
                fitnessCost += TSP.costMatrix[genes[i]][genes[i+1]];
                fitnessDistance += TSP.distanceMatrix[genes[i]][genes[i + 1]];
            }
        }

        public bool dominates(Individual a)
        {
            if ((this.fitnessDistance < a.fitnessDistance && this.fitnessCost <= a.fitnessCost) ||
                (this.fitnessDistance <= a.fitnessDistance && this.fitnessCost < a.fitnessCost))
            {
                return true;
            }
            return false;
        }

    }
}
