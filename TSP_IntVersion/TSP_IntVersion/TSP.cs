﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Windows.Forms.DataVisualization.Charting;

namespace TSP_IntVersion
{
    public partial class TSP : Form
    {
        public static Random randomize;
        public static int[][] distanceMatrix;
        public static int[][] costMatrix;
        public static int numberofCities;
        public static int numberofGenerations;
        public static int populationSize;
        public static double mutationRate;
        public static double crossoverRate;
        public static double tournamentSize;
        public Population population;

        public TSP()
        {
            InitializeComponent();
            randomize = new Random();
            chart1.Series.Clear();
            chart2.Series.Clear();
            chart3.Series.Clear();

        }
        public void initializeEverything()
        {
            if (!Int32.TryParse(citiesTextBox.Text, out numberofCities))
                numberofCities = 48;
            if (!Int32.TryParse(generationsTextBox.Text, out numberofGenerations))
                numberofGenerations = 1000;
            if (!Int32.TryParse(populationTextBox.Text, out populationSize))
                populationSize = 50;
            if (!Double.TryParse(mutationTextBox.Text, out mutationRate))
                mutationRate = 0.5;
            if (!Double.TryParse(crossoverTextBox.Text, out crossoverRate))
                crossoverRate = 1;
            if (!Double.TryParse(tournamentTextBox.Text, out tournamentSize))
                tournamentSize = 2;


            //numeracije gradova u Individuals ce ici od 1 do number of cities i direktno pristupamo matricama posto se nulte ignorisu

            distanceMatrix = new int[numberofCities + 1][]; //plus 1 jer ignorisemo prvu vrstu
            for (int i = 0; i <= numberofCities; i++)   // <= jer ignorisemo prvu kolonu
            {
                distanceMatrix[i] = new int[numberofCities + 1];  //plus 1 jer ignorisemo prvu kolonu
            }

            costMatrix = new int[numberofCities + 1][]; //isto vazi kao i za gornju matricu
            for (int i = 0; i <= numberofCities; i++)
            {
                costMatrix[i] = new int[numberofCities + 1];
            }

            WriteOutExcelFile("Distance.xlsx", "Sheet1", distanceMatrix);
            WriteOutExcelFile("Cost.xlsx", "Sheet1", costMatrix);

            population = new Population(populationSize, numberofCities);
            population.updateFitnessForAll();

            //obavezno pozivati i u while petlji
            population.calculateRank();
            population.calculateCrowdingDistance();
        }
        public void WriteOutExcelFile(string fileName, string sheetName, int[][] dataTable)
        {
            //Excel file mora da bude u bin->debug folderu da bi ovo radilo

            using (var document = SpreadsheetDocument.Open(fileName, isEditable: false))
            {
                var workbookPart = document.WorkbookPart;
                var sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
                var worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                var sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                int i = 0;
                foreach (var row in sheetData.Elements<Row>())
                {
                    int j = 0;
                    foreach (var cell in row.Elements<Cell>())
                    {
                        dataTable[i][j] = GetCellValue(cell);
                        if (i > j)
                        {
                            dataTable[j][i] = dataTable[i][j];
                        }
                        j++;
                    }
                    i++;
                    if (i + 1 > dataTable.Length)
                    {
                        return;
                    }
                }
            }
        }


        public int GetCellValue(Cell cell)
        {

            var value = cell.CellFormula != null
                ? cell.CellValue.InnerText
                : cell.InnerText.Trim();

            try
            {
                return Int32.Parse(value);
            }
            catch
            {
                return 0;
            }

        }

        private void startButton_Click(object sender, EventArgs e)
        {
            int i = 0;
            initializeEverything();

            while (i++ < numberofGenerations)
            {
                Population children = new Population(population);
                while (children.individuals.Count != population.individuals.Count)
                {
                    Individual a = new Individual(population.tournamentSelectionCrowdingDistance());
                    Individual b = new Individual(population.tournamentSelectionCrowdingDistance());
                    if (randomize.NextDouble() < crossoverRate)
                        a.crossover(b);
                    if (randomize.NextDouble() < mutationRate)
                        a.mutation();
                    children.individuals.Add(a);
                }
                children.updateFitnessForAll();

                //spojimo populacije
                foreach (var indiv in population.individuals)
                    children.individuals.Add(indiv);
                children.populationSize += population.populationSize;
                children.calculateRank();
                children.calculateCrowdingDistance();

                //smanjimo populaciju
                Population newPopulation = new Population(population);
                int j = 0;
                foreach (var front in children.fronts)
                    if (newPopulation.individuals.Count + front.Count <= newPopulation.populationSize)
                        foreach (var ind in front)
                            newPopulation.individuals.Add(ind);
                    else
                    {
                        List<Individual> temp = front.OrderBy(value => value.crowdingDistance).ToList();
                        while (newPopulation.individuals.Count < newPopulation.populationSize)
                            newPopulation.individuals.Add(temp[j++]);
                        break;
                    }

                population = newPopulation;
                population.calculateRank();
                population.calculateCrowdingDistance();
            }

            chartUpdate(chart1,dataGridView1,population.fronts.Count,population.individuals);
            chartUpdate(chart2,dataGridView2,1,population.fronts[0]); 
        }

        public void chartMinMaxUpdate(Individual individual, Chart chart, DataGridView dataGridView, string name)
        {
            chart.Series.Add(name);
            chart.Series[name].ChartType = SeriesChartType.Point;
            chart.Series[name].MarkerSize = 13;
            chart.Series[name].Points.AddXY(individual.fitnessDistance, individual.fitnessCost);
            dataGridView.Rows.Add(name,individual.fitnessDistance,individual.fitnessCost);
        }

        public void chartUpdate(Chart chart, DataGridView dataGridView, int ranksNumber, List<Individual> individuals)
        {
            Individual individual;
            chart.Series.Clear();
            for (int k = 0; k < ranksNumber; k++)
            {
                chart.Series.Add("Rank " + (k + 1)+": "+population.fronts[k].Count);
                chart.Series["Rank " + (k + 1) + ": " + population.fronts[k].Count].ChartType = SeriesChartType.Point;

                foreach (var node in population.fronts[k])
                {
                    chart.Series["Rank " + (k + 1) + ": " + population.fronts[k].Count].Points.AddXY(node.fitnessDistance, node.fitnessCost);
                }
            }

            dataGridView.Rows.Clear();
            individual = population.getCostMaxFit(individuals);
            chartMinMaxUpdate(individual, chart, dataGridView, "Max Cost");
            individual = population.getCostMinFit(individuals);
            chartMinMaxUpdate(individual, chart, dataGridView, "Min Cost");
            individual = population.getDistanceMaxFit(individuals);
            chartMinMaxUpdate(individual, chart, dataGridView, "Max Distance");
            individual = population.getDistanceMinFit(individuals);
            chartMinMaxUpdate(individual, chart, dataGridView, "Min Distance");
        }

        private void btnAddToChart_Click(object sender, EventArgs e)
        {
            //chart3
            chart3.Series.Add("Run " + (chart3.Series.Count + 1));
            chart3.Series["Run " + (chart3.Series.Count)].ChartType = SeriesChartType.Point;
            foreach (var node in population.fronts[0])
            {
                chart3.Series["Run " + (chart3.Series.Count)].Points.AddXY(node.fitnessDistance, node.fitnessCost);
            }
        }

        private void btnClearChart_Click(object sender, EventArgs e)
        {
            chart3.Series.Clear();
        }
    }
}
