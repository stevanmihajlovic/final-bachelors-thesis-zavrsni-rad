﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TSP_IntVersion
{
    public class Population
    {
        public List<Individual> individuals;
        public List<List<Individual>> fronts;
        public int populationSize;
        public int genomLength;

        public double distanceTotalFitness;
        public double distanceAverageFitness;
        public double distanceStandardDeviation;

        public double costTotalFitness;
        public double costAverageFitness;
        public double costStandardDeviation;

        public Population()
        {
            individuals = new List<Individual>();
            fronts = new List<List<Individual>>();
            populationSize = 0;
            genomLength = 0;
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
        }

        public Population(Population p)
        {
            populationSize = p.populationSize;
            genomLength = p.genomLength;
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
            individuals = new List<Individual>();
            fronts = new List<List<Individual>>();
        }
        public Population(int size, int n)
        {
            populationSize = size;
            genomLength = n;
            individuals = new List<Individual>();
            fronts = new List<List<Individual>>();
            distanceTotalFitness = 0;
            distanceAverageFitness = 0;
            distanceStandardDeviation = 0;
            costTotalFitness = 0;
            costAverageFitness = 0;
            costStandardDeviation = 0;
            for (int i = 0; i < populationSize; i++)
                individuals.Add(new Individual(genomLength));
        }
        public Individual getDistanceMaxFit(List<Individual> list)
        {
            Individual maxfit = list[0];
            for (int i = 1; i < list.Count; i++)
                if (maxfit.fitnessDistance < list[i].fitnessDistance)
                    maxfit = list[i];
            return maxfit;
        }
        public Individual getDistanceMinFit(List<Individual> list)
        {
            Individual minfit = list[0];
            for (int i = 1; i < list.Count; i++)
                if (minfit.fitnessDistance > list[i].fitnessDistance)
                    minfit = list[i];
            return minfit;
        }
        public Individual getCostMaxFit(List<Individual> list)
        {
            Individual maxfit = list[0];
            for (int i = 1; i < list.Count; i++)
                if (maxfit.fitnessCost < list[i].fitnessCost)
                    maxfit = list[i];
            return maxfit;
        }
        public Individual getCostMinFit(List<Individual> list)
        {
            Individual minfit = list[0];
            for (int i = 1; i < list.Count; i++)
                if (minfit.fitnessCost > list[i].fitnessCost)
                    minfit = list[i];
            return minfit;
        }
        public void updateFitnessForAll()
        {
            foreach (Individual i in individuals)
                i.calculateFitness();
        }
        public void updatePopulationData()
        {
            distanceTotalFitness = 0;
            costTotalFitness = 0;
            foreach (Individual i in individuals)
            {
                distanceTotalFitness += i.fitnessDistance;
                costTotalFitness += i.fitnessCost;
            }
            distanceAverageFitness = distanceTotalFitness / populationSize;
            costAverageFitness = costTotalFitness / populationSize;
            distanceStandardDeviation = 0;
            costStandardDeviation = 0;
            foreach (Individual i in individuals)
            {
                distanceStandardDeviation += Math.Pow(i.fitnessDistance - distanceAverageFitness, 2);
                costStandardDeviation += Math.Pow(i.fitnessCost - costAverageFitness, 2);
            }
            distanceStandardDeviation /= populationSize;
            costStandardDeviation /= populationSize;
            distanceStandardDeviation = Math.Sqrt(distanceStandardDeviation);
            costStandardDeviation = Math.Sqrt(costStandardDeviation);
        }

        public void calculateRank() 
        {
            List<Individual> currentFront = new List<Individual>();
            foreach (var p in individuals)
            {
                p.dominatedByCount = 0;
                p.dominatesList.Clear();
                foreach (var q in individuals)
                {
                    if (p.dominates(q))
                        p.dominatesList.Add(q);
                    else if (q.dominates(p))
                        p.dominatedByCount++;
                }
                if (p.dominatedByCount == 0)
                {
                    p.rank = 1;
                    currentFront.Add(p);
                }
            }
            int i = 1;

            while (currentFront.Count != 0)
            {
                fronts.Add(currentFront);
                List<Individual> newFront = new List<Individual>();
                foreach (var p in currentFront)
                {
                    foreach (var q in p.dominatesList)
                    {
                        q.dominatedByCount--;
                        if (q.dominatedByCount == 0)
                        {
                            q.rank = i + 1;
                            newFront.Add(q);
                        }
                    }
                }
                i++;
                currentFront = newFront;
            }
        }

        public void calculateCrowdingDistance()
        {
            foreach (var front in fronts)
            {
                foreach (var node in front)
                {
                    node.crowdingDistance = 0;
                }
                double fitnessMaxCost = getCostMaxFit(individuals).fitnessCost;
                double fitnessMinCost = getCostMinFit(individuals).fitnessCost;
                double fitnessMaxDistance = getDistanceMaxFit(individuals).fitnessDistance;
                double fitnessMinDistance = getDistanceMinFit(individuals).fitnessDistance;

                //po cost
                List<Individual> temp = front.OrderBy(value => value.fitnessCost).ToList();
                temp[0].crowdingDistance = Int32.MaxValue;
                temp[temp.Count - 1].crowdingDistance = Int32.MaxValue;

                for (int i = 2; i < temp.Count - 1; i++)
                {
                    temp[i].crowdingDistance = temp[i].crowdingDistance +
                                                (temp[i - 1].fitnessCost + temp[i + 1].fitnessCost) /
                                                (fitnessMaxCost - fitnessMinCost);
                }

                //po distance
                temp = front.OrderBy(value => value.fitnessDistance).ToList();
                temp[0].crowdingDistance = Int32.MaxValue;
                temp[temp.Count - 1].crowdingDistance = Int32.MaxValue;

                for (int i = 2; i < temp.Count - 1; i++)
                {
                    temp[i].crowdingDistance = temp[i].crowdingDistance +
                                                (temp[i - 1].fitnessDistance + temp[i + 1].fitnessDistance) /
                                                (fitnessMaxDistance - fitnessMinDistance);
                }
                temp = front.OrderBy(value => value.crowdingDistance).ToList();
            }
        }
        public Individual tournamentSelectionCrowdingDistance()
        {
            var randomAdults = new List<Individual>();
            for (var i = 0; i < TSP.tournamentSize; i++)
            {
                randomAdults.Add(individuals[TSP.randomize.Next(populationSize)]);
            }
            int minRank = randomAdults[0].rank;
            for (int i = 1; i < randomAdults.Count; i++)
            {
                if (minRank > randomAdults[i].rank)
                    minRank = randomAdults[i].rank;
            }

            var bestRankedAdults = new List<Individual>();
            foreach (var i in randomAdults)
                if (i.rank == minRank)
                    bestRankedAdults.Add(i);
            bestRankedAdults = bestRankedAdults.OrderByDescending(value => value.crowdingDistance).ToList();
            return randomAdults[0]; 
        }
    }
}
